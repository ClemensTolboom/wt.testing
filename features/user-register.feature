Feature: Visibility of the homepage.
  In order to check the registration process
  As a site administrator
  I want to verify I can register as a new user

  Scenario: Register a new Account
    Given I am on the homepage
    When I fill in "fname" with "firstname"
    And I fill in "lname" with "lastname"
    And I fill in "email" with "email"
    And I fill in "password" with "password"
    # to check for the existence of given fields
    Then I give screenrecorder 2 seconds
