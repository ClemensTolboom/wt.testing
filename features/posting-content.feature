Feature: As a user I want to post content.
  In order to read content we need to post it first
  and het would be nice the text looks as expected

  Scenario: In order to post content we need a sub wiki
    Given I am logged in to WTS
    And I am on "wt/behat-testing"
    # created manually. Should be step 'I visit "wt/behat-testing"'
    Then I wait for 1 seconds
    # to let page iframe be ready
    When I insert into wysiwyg field "<b>Hello world</b>"
    And I click on the element with xpath "//*[@id='saveArticle']"
    Then I wait for 1 seconds
    Then I should see "hello world"
    Then I give screenrecorder 5 seconds
