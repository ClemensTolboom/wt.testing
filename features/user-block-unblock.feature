Feature: How to block and unblock a user.
  When blocking a user there must be an unblock too.

  Scenario: Block a user
    Given I am logged in to WTS
    Then I wait for 2 seconds
    # to make page get loaded

    When I visit the user page of "block-user"
    And I give screenrecorder 5 seconds
    Then the response status code should be 200

    # https://stackoverflow.com/questions/8808921/selecting-a-css-class-with-xpath#9133579
    # And I click on the element with xpath "//*[@id=user-profile]/div/div[2]/div[1]/div[2]/span[contains(concat(' ', normalize-space(@class), ' '), ' user2userblockoptions ')]/a"
    And I click on the element with xpath "//span[@class='user2userblockoptions']/a"

    # FIXME: A dialog appears which need an OK click
    And I give screenrecorder 5 seconds

    And I wait for 1 seconds
    # for the data to be processed

    Then I am on "myaccount#blocks"
    # This should be the NOT variant. Fix through issue #14.
    #And I should not see "No User blocked by you"
    And I should see "No User blocked by you"
    And I give screenrecorder 5 seconds
