Feature: Login with an existing account.
  In order to check for login and logout we want to
  check for the user home page for logged-in user.

  Scenario: Login to WT.Social using the default user
    Given I am logged in to WTS
    Then I wait for 2 seconds
    # to make page get loaded
    Then I should see "MY SUBWIKI MEMBERSHIPS"
    And I should see "SUBWIKIS TO JOIN"
    # And I should be on "onboarding"
    Then I give screenrecorder 5 seconds
    When I logout from WTS
    And I wait for 2 seconds
    Then the response status code should be 200
    And I should see "Login"
    And I should see "No account?"
    Then I give screenrecorder 5 seconds
