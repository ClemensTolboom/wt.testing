Feature: Visibility of the homepage.
  In order to have confidence that my site is accessible
  As a site administrator
  I want to verify I can visit the homepage

  Scenario: Verify the homepage.
    Given I am on the homepage
    Then the response status code should be 200
    And I should see "Already a member?"
    And I should see "Login"
    # Useful when writing tests to see the page
    Then I wait for 5 seconds
