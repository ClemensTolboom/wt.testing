Feature: Login with an existing account.
  In order to login we want to check for the fields

  Scenario: Login with an existing Account.
    Given I am on "login"
    Then I fill in "email" with "email"
    And I fill in "password" with "password"
    # to check for given fields

  Scenario: Visiting /logout as anonymous is a bad idea
    Given I am on "logout"
    # and assuming I was not logged in before
    Then the response status code should be 200
    And I should see "Already a member?"
    And I should see "Login"
    And I should see "Register"
    # and the fragment `/?redirect=logout` is a bad redirect
    Then I give screenrecorder 2 seconds
