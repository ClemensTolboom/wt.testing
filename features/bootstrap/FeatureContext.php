<?php

use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;
use Behat\MinkExtension\Context\MinkContext;
use Symfony\Component\Console\Exception\MissingInputException;

class FeatureContext extends MinkContext {

  private $config;

  /**
   * @BeforeScenario
   */
  public function loadConfig(BeforeScenarioScope $scope) {
    $environment = $scope->getEnvironment();

    $file = "config.json";
    if (!file_exists($file)) {
      throw new InvalidArgumentException('File "' . $file . '" not find.');
    }

    $content = file_get_contents($file);
    $json = json_decode($content);
    if (empty($json)) {
      throw new InvalidArgumentException("Invalid " . $file . ' json content. Check it for typos.');
    }

    $this->config = $json;
  }


  /**
   * @Given I am logged in to WTS
   */
  public function iAmLoggedInToWTS() {
    // FIXME: get base URL from context
    $this->getSession()->visit('https://wtsdev.wikitribune.com/login');
    $page = $this->getSession()->getPage();

    $config = $this->getDefaultUser();
    if (!isset($config->email)) {
      throw new MissingInputException('Config does not have "defaults/user/email" set.');
    }
    if (!isset($config->password)) {
      throw new MissingInputException('Config does not have "defaults/user/password" set.');
    }

    /** @var NodeElement */
    $emailField = $page->find('xpath', '//input[@name="email"]');
    $emailField->setValue($config->email);

    /** @var NodeElement */
    $passwordField = $page->find('xpath', '//input[@name="password"]');
    $passwordField->setValue($config->password);

    $submitField = $page->find('xpath', '//button[@type="submit"]');
    $submitField->click();

  }

  /**
   * Returns config.json/defaults/user if available.
   *
   * @throws MissingInputException
   */
  private function getDefaultUser() {
    $config = $this->getConfig()->defaults;
    if (!isset($config->user)) {
      throw new MissingInputException('Config does not have "defaults/user" set.');
    }
    return $config->user;
  }

  /**
   * Returns config.json/defaults/accounts.
   *
   * @throws MissingInputException
   */
  private function getDefaultAccounts() {
    $config = $this->getConfig()->defaults;
    if (!isset($config->accounts)) {
      throw new MissingInputException('Config does not have "defaults/accounts" set.');
    }
    return $config->accounts;
  }

  private function getConfig() {
    $config = $this->config;
    if (!$config) {
      throw new MissingInputException('Config does not have any value. Check config.json.dist');
    }
    if (!isset($config->defaults)) {
      throw new MissingInputException('Config does not have "defaults" set.');
    }
    return $this->config;
  }

  /**
   * @When I logout from WTS
   */
  public function iLogoutFromWTS() {
    $this->visit('logout');
  }

  /**
   * @When I visit the user page of :arg1
   */
  public function iVisitTheUserPageOf($arg1)
  {
      // Check for a configured user
      $config = $this->getDefaultAccounts();
      if (isset($config->{$arg1})) {
        $arg1 = $config->{$arg1}->uid;
      }
      $this->visit('u/' . $arg1);
  }

  public function visit($path) {
    // FIXME: get base URL from context
    $this->getSession()->visit('https://wtsdev.wikitribune.com/' . $path);
  }

  /**
   * @Then I wait for :arg1 seconds
   */
  public function iWaitForSeconds($arg1) {
    $seconds = intval($arg1);
    sleep($seconds);
  }

  /** Click on the element with the provided xpath query
   *
   * @When /^I click on the element with xpath "([^"]*)"$/
   *
   * @see https://stackoverflow.com/questions/15182000/behat-mink-unable-to-simulate-click-on-button-in-footer
   */
  public function iClickOnTheElementWithXPath($xpath) {
    $session = $this->getSession();
    $element = $session->getPage()->find(
      'xpath',
      $session->getSelectorsHandler()->selectorToXpath('xpath', $xpath)
    );

    if (NULL === $element) {
      throw new InvalidArgumentException(sprintf('Could not evaluate XPath: "%s"', $xpath));
    }

    /** @var DocumentElement */
    $element->click();

  }

  /**
   * @When I insert into wysiwyg field :arg1
   */
  public function iInsertIntoWysiwygField($arg1) {
    $this->getSession()->getDriver()->evaluateScript(
      <<<EOF
tinymce.activeEditor.execCommand('mceInsertContent', false, " $arg1 ");
EOF
    );

  }

  /**
   * @Then /^I give screenrecorder (\d+) seconds$/
   */
  public function iGiveScreenrecorderSeconds($arg1) {
    $config = $this->getConfig()->defaults;
    if (!isset($config->screenrecorder)) {
      throw new MissingInputException('Config does not have "defaults/screenrecorder" set.');
    }
    if (!isset($config->screenrecorder->enabled)) {
      throw new MissingInputException('Config does not have "defaults/screenrecorder/enabled" set.');
    }
    if (!$config->screenrecorder->enabled) {
      return;
    }

    $seconds = intval($arg1);
    sleep($seconds);
  }

  /**
   * @AfterStep
   */
  public function takeScreenShotAfterFailedStep(AfterStepScope $scope) {
    if (99 === $scope->getTestResult()->getResultCode()) {

      $filename = $scope->getFeature()->getFile();
      $filename = basename($filename);
      $filename = str_replace('.feature', '.png', $filename);
      $path = __DIR__ . '/../../reports/assets/screenshots/';
      $filename = $path . $filename;

      $this->getSession()->getDriver()->resizeWindow(1024, 768);
      sleep(0.2);
      file_put_contents($filename, $this->getSession()
                                        ->getDriver()
                                        ->getScreenshot());
    }
  }

}
