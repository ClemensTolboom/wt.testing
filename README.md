# Using Behat on WT.Social

Using Behat we can write functional tests using plain English. For some sentences we need to write some code in PHP.

```behat
Feature: Login with an existing account.
  In order to check for login and logout we want to
  check for the user home page for logged-in user.

  Scenario: Login to WT.Social using the default user
    Given I am logged in to WTS
    Then I wait for 2 seconds
    # to make page get loaded
    Then I should see "MY SUBWIKI MEMBERSHIPS"
    And I should see "SUBWIKIS TO JOIN"
    # And I should be on "onboarding"
    Then I give screenrecorder 5 seconds
    When I logout from WTS
    And I wait for 2 seconds
    Then the response status code should be 200
    And I should see "Login"
    And I should see "No account?"
    Then I give screenrecorder 5 seconds
```

## Getting started

- [ ] You need to have Chromium or Google Chome installed.
- [ ] Make sure you have PHP installed.
- [ ] Make sure you have [composer](https://getcomposer.org/download/) installed.
- [ ] Download this repo.
- [ ] Run `composer install` to get all dependencies needed for base dependencies in `composer.json`.
- [ ] Copy `config.json.dist` to `config.json` and fill in valid user credential for `/defaults/user/*`.

## Useful commands

```
# List of patterns
vendor/bin/behat --definitions i

# Condens list of patterns
vendor/bin/behat --definitions l

vendor/bin/behat --definitions i | grep button
```

## Start your browser in debug mode

Make sure Chromium or Chrome is not running yet. Once running by the command below you can start develop your tests.

### Chromium

```bash
# Mac
chromium --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222

# Alpine linux
chromium-browser --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
```

### Chrome

```bash
chrome --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
```

## Run tests

### Run all tests

```bash
vendor/bin/behat
```

### Run a particular test

```bash
vendor/bin/behat features/user-login.feature 
```

## Resources

### Drivers

From the list of drivers we choose Chrome as easy to install and fast drivers. No Java required.

```
behat/mink suggests installing behat/mink-selenium2-driver (slow, but JS-enabled driver for any app (requires Selenium2))
behat/mink suggests installing dmore/chrome-mink-driver (fast and JS-enabled driver for any app (requires chromium or google chrome))
behat/mink suggests installing behat/mink-zombie-driver (fast and JS-enabled headless driver for any app (requires node.js))
```

### Sites

#### Writing tests

- https://trello.com/b/4xcMYlzV/wtsocial-backlog to find QA card you can write tests for.
- https://behat.org/en/latest/index.html

#### Others

- https://github.com/bilkoh/wt-social-tests/blob/master/test_posts.py Python version triggering this PHP version.
- https://github.com/Behat/MinkExtension/issues/337
- https://gitlab.com/DMore/chrome-mink-driver/blob/master/README.md
- https://github.com/dutchiexl/BehatHtmlFormatterPlugin (not used yet; buggy?; tried `dev-master`)

# Docker

## Shell for debugging purposes.

```
docker-compose run behat
```
